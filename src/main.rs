mod commands;

use std::env;
use commands::{Command, command_to_string, print_help_text, string_to_command};

fn main() {
    let args: Vec<String> = env::args().collect();
    let command_string = &args[1];

    let command = string_to_command(command_string.trim_end());

    println!("You entered the command: {}", command_to_string(command));

    match command {
        Command::LIST => println!("List all books"),
        Command::HELP | Command::UNKNOWN => print_help_text(),
    }
}
