use strum::IntoEnumIterator;
use strum_macros::EnumIter;

#[derive(EnumIter)]
pub enum Command {
    LIST,
    HELP,
    UNKNOWN,
}

impl Copy for Command {}
impl Clone for Command {
    fn clone(&self) -> Command {
        *self
    }
}

fn command_to_help_text_mapping(cmd: &Command) -> &str {
    return match cmd {
        Command::LIST => "\tlist\tlists all books",
        Command::HELP => "\thelp\tthis hellp menu",
        Command::UNKNOWN => ""
    }
}

pub fn string_to_command(string: &str) -> Command {
    return match string {
        "list" => Command::LIST,
        "help" => Command::HELP,
        _ => Command::UNKNOWN,
    };
}

pub fn print_help_text() {
    let name = env!("CARGO_PKG_NAME");
    let version = env!("CARGO_PKG_VERSION");
    println!("{}, v{}", name, version);
    println!("Usage:");
    for command in Command::iter() {
        println!("{}", command_to_help_text_mapping(&command))
    }
}

pub fn command_to_string(command: Command) -> String {
    return match command {
        Command::LIST => String::from("list"),
        Command::HELP => String::from("help"),
        Command::UNKNOWN => String::from("unknown"),
    };
}
